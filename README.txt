This module extends the Video (http://drupal.org/project/video) module by
providing a "Multi-source video player" field formatter.

This field formatter works in exactly the same way as the original "Video
player" formatter, except when the video field contains multiple values. In this
case, each video will be added as a separate "source" element of the rendered
HTML5 "video" element.
